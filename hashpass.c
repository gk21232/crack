#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"

int main(int argc, char *argv[])
{
    
    if(argc !=3)
    {
        printf("Invalid");
    }
      
    FILE *in;
    in = fopen(argv[1], "r");
    if(!in)
    {
        printf("Can't open");
        exit(1);
    }
    FILE *out;
    out = fopen(argv[2],"w");   
        if(!out)
        {
            printf("Can't open file");
            exit(1);
        }
    
    char word[20];
    char line[50]; 
    char *hash;
    while ( fgets(line, 50, in)!= NULL)
    {
        int filled = sscanf(line, "%s", word);
        if( filled == 1)
        {
            hash = md5(word, strlen(word));
            fprintf(out, "%s\n", hash);
        }
       free(hash);
    }
    
    fclose(in);
    
    fclose(out);
        

}